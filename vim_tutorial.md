## Discussion:
- buffers/windows
- swap files
- ctags/cscope/jumping
- this file
- git


### Prerequisites

1) Install:
```
$ sudo apt install vim-gtk
```

2) Configure:
```
$ vim ~/.vimrc
```
[Refer to my .vimrc](https://gitlab.com/divyesh.vartha/rice-soup/-/blob/master/ubuntu20/.vimrc)

3) `$ vimtutor`

### Modes

Most common modes you need to know

| Mode | Use |
|  --- |  ----|
| Normal | Navigation + key-defined commands |
| Insert | Text Editing |
| Visual | Selection |
| Command| File operations, help, set vim configs, complex changes like search & replace|
|Explorer| Navigate directories, create files, rename |

- To get help about anything, go to normal mode, type `:help <topic>` for help on any topic. This will open a help window, to close that window, type `:q` and press Enter.
  - For example, to know what pressing `CTRL-d` does, write `:help ctrl-d`

### save & quit

1) Press ESC to enter Normal from other modes, in most cases.

2) Then type:
- `:q`  to quit
- `:q!` to quit losing any unsaved changes
- `:w`  to save (should have write permission)
- `:wq` to save and quit

Alternatives:
- `:x` or `ZZ (no colon)` to write only if changes are made. (does not modify last modified timestamp unlike `:wq)`
- `ZQ` to quit without saving (same as `:q!`)

### Normal mode: Navigation


1) Basic movement keys are `h`, `j`, `k` and `l`.

2) Within a line, use:
```
w               W
b               B
0               ^               $
```

- Similar but not often used:
```
e               E
ge              gE
```

3) Within a file, use:
```
gg              G
Ctrl-u          Ctrl-d
:5              jump to 5th line
5G              same as above
''              jump to last jumped position
%               (jump cursor to other end of brackets, #if directives, and /* */ style comments )
```

### Normal mode: Functions
In normal mode, aside from movement keys, most other keys also have some functions.
```
key             function

i               go to insert mode
u               for undo (CTRL-R will redo)
p       P       will paste after the cursor, before the cursor
q <key>         will start a recording into <key> buffer (press q again to stop)
fq	Fq	will find the next letter q in the current line (f for left to right, F for vice versa)
;               (after find) jump to next find match
tq	Tq	same as find but same with minor diff
Y               yank(copy) whole line
.               repeat last edit
~               switch case
>>      <<      Increase/decrease indentation of line (can also select multiple lines)
zt      zb      Scroll window top/bottom without moving cursor
zz              Scroll window so that cursor is at the center of the screen
```

Search:
```
/       ?       Enter search mode. Type your pattern and press enter.
n       N       Go to next or previous match
*       #       Go to next occurrence of the text under cursor
```

-------------------------------------------------------------------------------------------------
### Insert mode:
To go into insert mode, press `i`. Here are some alternatives to try:
```
i	I       Insert
a	A       Append
o	O       Insert line above or below cursor
s	S       Substitute
c	C       Change
```

These can also be used to edit without going into Insert mode
```
x	dd	D
r	R
```

### Visual mode:
- To go into visual mode, from normal mode use the below.
```
v       Normal Visual Selection (char. to char.)
V       Visual Line
CTRL-v  Visual Block
```
- Most common thing to do after selection is to copy (yank) by pressing `y` or to delete using `x.`
- Some other common actions are: `p` or `s` or `=`

```
 =     Auto indent selected lines
```
### Combining movements/actions with numbers
Many commands that can be prepended with a number to multiply their effect. Eg: `3dd` will delete three lines, `3w` will be as if you pressed `w` three times, `c3w` or `3cw` will "change" 3 words, `3n`

### Command mode:
You can use arrow keys to view older commands. `CTRL-F` while in cmd mode will open a list, `:q` to exit that.


An important function you will require is search and replace patterns. See `:help substitute`

```:% s/old_name/new_name/gc```
- The first parameter is the range, `%` represents the whole file, you could also replace it with `30,40` to instead operate only on lines 30 to 40.
- The `s` is short for substitute command
- `old_name` is the string that will be search for, `new_name` is the string that will replace the match
- The last parameters are flags
  - `g` will replace all occurrences in a line. (Default will only search replace the first match in a line)
  - `c` will ask you for confirmation before each replace

The substitute command is quite versatile (search pattern can also be regex) but variations of the above example should cover most of your needs.

## Misc topics:
### netrw Explorer

```
:E(xplore)      opens a file browser, if current file is unsaved,
                it will open this in a new split window.
:Ve(xplore)     same but in a vertical split window

%       create a new file
d       create a new directory

D       delete
R       rename
```

### ctags

```
$ ctags -R .                    // Generate ctags in current folder after recursively traversing all directories
$ ctags -L file_list.txt     // Generate for files/directories specified in the file 
$ ctags -L -                 // Same as above but takes input from stdin

Eg: $ find . -name \*.c -o -name \*.h | ctags -L -
    $ ls main/*.c main/*.h | ctags -L -
```

`CTRL-]` will jump into definition:

`CTRL-T` will come out of definition: (Strictly speaking, go to older entry in tag stack, see `:help CTRL-T`)

### Jumps

`:help jumplist`

```
CTRL-I          Next jump
CTRL-O          Previous jump
```

### Buffers:
```
:ls             display loaded buffer list

:bn             open next buffer in buffer list
:bp             open previous buffer

:b filename     search and open filename from list. (Tab completion available)

:bd 1 2         unload buffers 1 and 2
:1,100bd        unload buffers from 1 to 100
```

### Windows:

```
:sp(lit)        splits the window horizontally. Alt: CTRL-w s
:vsp(lit)       splits the window vertically. Alt: CTRL-w v  
CTRL-w w        cycle through the windows clockwise
CTRL-w h,j,k,l  jump to window on left, above, below or right
CTRL-w H,J,K,L  With capital H,J,K,L to arrange windows
CTRL-w =        equalise window size
```

### Marks
```
:help marks

m{a-zA-Z}               Set mark {a-zA-Z} at cursor position

'{a-zA-Z}               Jump to line marked {a-zA-Z}

`{a-zA-Z}               Jump to exact marked character
```

***'*** (single quote) and ***`*** (backtick) are special marks. They store the previous position contexts, jump b/w the current and last cursor position using ***''*** (single quote x2) or ***``*** (backtick x2)


Generally, marks can also be used as part of a motion command. Eg: `V'k` is visual line select from current line to the line marked by mark k

Lowercase {a-z} marks are local to each buffer/file and temporary.
Uppercase {A-Z} marks are global and are persistent.

So you can use uppercase marks as bookmarks of files which can be accessed from any vim process anytime. Note: Newly set global marks only become effective after you quit vim.

Tip 💡: If you use global mark to open a file, vim's pwd can be different from the file's working dir. Use

`: cd %:p:h`

% gives the name of the current file, %:p gives its full path, and %:p:h gives its directory

### Registers:
Vim stores the list of recently yanked or deleted text in a set of registers {0-9}. To see what is stored across all registers use the `:reg` command.

All keys {a-z} can also be used as registers to store any text. Select any text, press `"ay` and the text has been stored in register `a`.
To paste from this register, use `"ap`

The `""` register has no name, it is similar to the clipboard. Whatever is copied or deleted most recently is placed in the `""` register automatically. When you press just `p`, the text printed will be from this register.
The numbered registers 0-9 keep a stack of recently copied or deleted text.

(Key recordings recorded by `q` are also stored in the {a-z} registers. Take care not to accidently use same keys to avoid conflict)

Tip 💡: Use `"0p` for pasting copied content. If you delete something after copying something, doing just `p` will paste the deleted text instead since deleted text also automatically goes to `""` register. This replaces any copied text there. The original copied text is register 0 so use `"0p` to paste.

The `+` register is a special register for the system clipboard. This allows you bidirectional copy and paste between Vim and the system clipboard.

For example, use `"+Y` to copy current line to the system clipboard, and `"+p` to paste from clipboard to vim buffer.

For system clipboard, Vim requires the clipboard support to be compiled in. Check for clipboard support using `:echo has('clipboard')` or `$ vim --version`, if the result is 0 or -clipboard then your version does not have it compiled. For Linux, try installing vim-gtk or similar GUI related package which includes it.

### Vim Diff

```
(from shell)
$ vimdiff <file1> <file2>
```

```
(from within vim)
:diffsplit <file2>
:vert diffsplit <file2>
```

### Folding code blocks

https://unix.stackexchange.com/questions/141097/how-to-enable-and-use-code-folding-in-vim


### Misc tips and resources
💡: Time travel. See `:help earlier` and `:help later`. `:earlier 30m` will take you back to the state of the current file 30 minutes earlier. (does not work if the buffer has been closed i.e. in case of quiting vim)

💡: Swap files. They are temporary files which are created by vim while you have been editing a file. It can be used for file recovery in case the system shuts down without the file being properly saved. Upon successful exits, created swap files vanish on their own. You only need to manually deal with swap files when abnormal exits happen.

💡: Note that using `u` and `CTRL-R` will not get you to all past text states, while repeating `g-` and `g+` does.

💡: If at any point, your terminal gets "stuck", you may have accidently pressed `Ctrl-s.` Press `CTRL-q` to restore it. This can happen anywhere in the terminal but is more common to accidently do in vim.
💡: Install vim-airline plugin for a neat status bar. [vim-airline](https://github.com/vim-airline/vim-airline?tab=readme-ov-file)
💡: CTRL-A will increment the number below the cursor by one. Can be very helpful when making a recording dealing with increasing numbers. CTRL-X decrements.

Further reading links:
- [work-with-vim](https://mkaz.blog/working-with-vim/)
- [grokking-vim](https://stackoverflow.com/questions/1218390/what-is-your-most-productive-shortcut-with-vim/1220118#1220118)
